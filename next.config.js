const { i18n } = require('./next-i18next.config');

module.exports = {
  reactStrictMode: true,
  i18n,
  images: {
    domains: ['s3-us-west-2.amazonaws.com', 'www.notion.so'],
  },
}