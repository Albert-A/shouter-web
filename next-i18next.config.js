module.exports = {
  i18n: {
    defaultLocale: 'da',
    locales: ['en', 'da', 'sv'],
  },
  react: { useSuspense: false },//this line
};
