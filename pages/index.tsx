import type { NextPage } from "next";
import Head from "next/head";
import styled from "styled-components";
import Icon from "../components/Icon/Icon";
import Link from "next/dist/client/link";
import { useTranslation } from "next-i18next";
import Typography from "../components/Typography/Typography";
import Toggle from "../components/Toggle/Toggle";
import theme from "../lib/theme";
import { useEffect, useRef, useState } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import useMobileDetect from "../lib/use-mobile-detect";
import { Content } from "../components/Content/Content";
import { GetAppForm } from "../components/GetAppForm/GetAppForm";

export async function getServerSideProps(context: any) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["translation"])),
    },
  };
}

const Home: NextPage = () => {
  // Hooks
  const { t } = useTranslation();
  const currentDevice = useMobileDetect();

  // States
  const [isMobile, setisMobile] = useState<boolean>(false);
  const [showGetApp, setShowGetApp] = useState<boolean>(false);

  useEffect(() => {
    setisMobile(currentDevice.isMobile());
  }, [currentDevice]);

  // States
  const [isShouter, setIsShouter] = useState<boolean>(true);

  const GetAppMobile = () => {
    window.open(
      "https://apps.apple.com/dk/app/shouter-find-work-get-help/id1581976557"
    );
  };

  return (
    <>
      {showGetApp && (
        <div style={{ top: 0 }}>
          <GetAppForm
            onClose={() => {
              setShowGetApp(false);
            }}
          />
        </div>
      )}

      <Head>
        <title>Shouter</title>
        <meta name="landing" content="Shouter landing page" />
      </Head>

      <Main style={{ marginBottom: isMobile ? 100 : 200 }}>
        <BakgroundGradient isShouter={isShouter} />
        <LandingText>
          <div style={{ maxWidth: 550 }}>
            <Typography size={isMobile ? 30 : 60} font="bold">
              {isShouter
                ? t("translation:titleShouter")
                : t("translation:titleDoer")}
            </Typography>
          </div>
          <SubTitleText>
            <Typography
              size={theme.fontSizes.medium}
              spacingBefore={theme.spacings.xlarge}
            >
              {isShouter
                ? t("translation:subtitleShouter")
                : t("translation:subtitleDoer")}
            </Typography>
          </SubTitleText>
        </LandingText>

        <IconContainer style={{ marginTop: isMobile ? 70 : 20 }}>
          {isMobile && (
            <>
              <TopLine>
                <Icon id="mLineTop" />
              </TopLine>
              <BottomLine>
                <Icon id="mLineBottom" />
              </BottomLine>
            </>
          )}
          <Icon id={isMobile ? "mouthSmall" : "mouth"} />
        </IconContainer>

        <div style={{ marginTop: isMobile ? 60 : 0 }}>
          <Toggle
            onClick={() => {
              setIsShouter(!isShouter);
            }}
          />
        </div>

        <ModeTextMobile>
          <Typography
            spacingBefore={50}
            align="center"
            size={theme.fontSizes.subHeader}
            font="bold"
          >
            {isShouter
              ? t("translation:shouterModeMobile")
              : t("translation:doerModeMobile")}
          </Typography>
          {isMobile && (
            <Typography
              spacingBefore={15}
              align="center"
              size={theme.fontSizes.small}
            >
              {isShouter
                ? t("translation:subtitleShouter")
                : t("translation:subtitleDoer")}
            </Typography>
          )}
        </ModeTextMobile>

        {!isMobile && (
          <>
            <DownloadLink>
              <DownloadLinkContainer>
                <div style={{ maxWidth: 65 }}>
                  <Typography align="right" size={theme.fontSizes.data + 1}>
                    {t("translation:googlePlay")}
                  </Typography>
                </div>
                <VerticalSeparator />
                <button
                  onClick={() => {
                    isMobile ? GetAppMobile() : setShowGetApp(true);
                  }}
                >
                  <AppStore rel="noopener" target="_blank">
                    <Icon id="appStore" />
                  </AppStore>
                </button>
              </DownloadLinkContainer>
            </DownloadLink>

            <ModeText>
              <Typography
                spacingBefore={15}
                align="center"
                size={theme.fontSizes.medium}
              >
                {isShouter
                  ? t("translation:shouterModeText")
                  : t("translation:doerModeText")}
              </Typography>
            </ModeText>
          </>
        )}

        <Content isShouter={isShouter} />
      </Main>
    </>
  );
};

export default Home;

export const Main = styled.main`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  padding: 0 150px;
  overflow: hidden;

  @media (max-width: 1000px) {
    padding: 0 100px;
  }

  @media (max-width: 850px) {
    padding: 0 80px;
  }

  @media (max-width: 600px) {
    padding: 0 40px;
  }

  @media (max-width: 500px) {
    padding: 0 20px;
  }
`;

export const BakgroundGradient = styled.div<{
  isShouter: boolean;
}>`
  position: absolute;
  z-index: -10;
  top: 300px;
  height: 1000px;
  width: 85%;
  background: ${({ isShouter }) =>
    isShouter
      ? "linear-gradient(180deg, #ffb2e1 0%, #da8fbc 100%)"
      : "linear-gradient(180deg, #ffb470 0%, #FF8337 100%)"};
  filter: blur(500px);
  backdrop-filter: 0;

  -webkit-backdrop-filter: blur(0px);

  @media (max-width: 1100px) {
    top: 200px;
    height: 90%;
    width: 75%;
    filter: blur(220px);
  }

  @media (max-width: 900px) {
    top: 200px;
    height: 90%;
    width: 75%;
    filter: blur(180px);
  }

  @media (max-width: 600px) {
    top: 180px;
    height: 85%;
    width: 75%;
    filter: blur(130px);
  }

  @media (max-width: 500px) {
    top: 150px;
    height: 90%;
    width: 90%;
    filter: blur(110px);
  }

  @media (max-width: 400px) {
    top: 150px;
    height: 100%;
    width: 90%;
    filter: blur(90px);
  }
`;

export const LandingText = styled.div`
  width: 100%;
  height: 220px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 100px;

  @media (max-width: 900px) {
    margin-top: 100px;
    max-width: 290px;
    height: 140px;
  }
`;

export const SubTitleText = styled.div`
  max-width: 390px;
  @media (max-width: 900px) {
    display: none;
  }
`;

export const LandingTextSmall = styled.div`
  display: none;
  margin-top: 90px;
  max-width: 290px;

  @media (max-width: 900px) {
    display: flex;
  }
`;

export const IconContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const TopLine = styled.div`
  position: absolute;
  right: 40px;
  z-index: -1;
  padding-bottom: 280px;
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: 0.3;

  @media (min-width: 500px) {
    display: none;
  }
`;

export const BottomLine = styled.div`
  position: absolute;
  left: 60px;
  z-index: -1;
  padding-top: 280px;
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: 0.3;

  @media (min-width: 500px) {
    display: none;
  }
`;

export const DownloadLink = styled.div`
  position: absolute;
  width: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding: 0 90px;
  bottom: ${({ theme }) => theme.spacings.xlarge}px;

  @media (max-width: 1100px) {
    display: none;
  }

  @media (min-width: 1500px) {
    position: fixed;
    padding: 0;
  }
`;

export const DownloadLinkContainer = styled.div`
  margin-right: 50px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: ${({ theme }) => theme.spacings.xxlarge}px;

  @media (min-width: 1500px) {
    margin-right: 80px;
  }
`;

export const VerticalSeparator = styled.div`
  height: 100%;
  width: 1px;
  margin-left: ${({ theme }) => theme.spacings.large}px;
  margin-right: ${({ theme }) => theme.spacings.large}px;
  background: ${({ theme }) => theme.colors.grey5};
`;

export const AppStore = styled.a`
  &:hover {
    cursor: pointer;
  }
`;

export const ModeText = styled.div`
  max-width: 440px;
`;

export const ModeTextMobile = styled.div`
  display: flex;
  max-width: 260px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
