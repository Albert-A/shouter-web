import '../styles/globals.css';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { ThemeProvider } from 'styled-components';
import theme from '../lib/theme';
import { LiveChatLoaderProvider, Intercom } from 'react-live-chat-loader';
import { appWithTranslation, useTranslation } from 'next-i18next';
import GlobalStyle from '../styles/global';
import { Router } from 'next/dist/client/router';
import styled from 'styled-components';
import Icon from '../components/Icon/Icon';
import { useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import { CookieContainer } from '../components/CookieContainer/CookieContainer';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import useMobileDetect from '../lib/use-mobile-detect';
import { GetAppForm } from '../components/GetAppForm/GetAppForm';
import '../i18n';

// Router.events.on('routeChangeComplete', (url) => {
//   // @ts-ignore window.analytics undefined below
//   window.analytics.page(url);
// });

function MyApp({ Component, pageProps }: AppProps) {
  const currentDevice = useMobileDetect();
  const hasCookieConsent = Cookies.get('cookieConsent');
  const coockieExpireDate: Date = new Date(
    new Date().getTime() + 7 * 24 * 60 * 60 * 1000,
  );

  // States
  const [showCookie, setShowCookie] = useState<boolean>(false);
  const [showGetApp, setShowGetApp] = useState<boolean>(false);
  const [isMobile, setisMobile] = useState<boolean>(false);

  const GetAppMobile = () => {
    if (currentDevice.isIos() === true) {
      window.open(
        'itms-apps://apps.apple.com/dk/app/shouter-find-work-get-help/id1581976557',
        '_blank',
      );
    } else {
      window.open(
        'https://apps.apple.com/dk/app/shouter-find-work-get-help/id1581976557',
        '_blank',
      );
    }
  };

  useEffect(() => {
    setisMobile(currentDevice.isMobile());
  }, [currentDevice]);

  useEffect(() => {
    hasCookieConsent
      ? setShowCookie(false)
      : setTimeout(() => {
          setShowCookie(true);
        }, 1600);
  }, [hasCookieConsent]);

  return (
    <>
      <GlobalStyle />
      <TopBlure />
      <ThemeProvider theme={theme}>
        <Head>
          <link rel="shortcut icon" href="/assets/favicon.png" />
          <title>Shouter</title>
          <meta charSet="UTF-8" />
          <meta
            name="description"
            content="Shout your needs and make cool money"
          />
          <meta name="keywords" content="HTML, CSS, JavaScript" />
          <meta name="author" content="Shouter" />
          <meta
            name="apple-itunes-app"
            content="app-id=1581976557, app-argument=shouter.dk"
          />
        </Head>
        <LiveChatLoaderProvider providerKey="r4vt8zfb" provider="intercom">
          <Contet>
            {showCookie && (
              <CookieContainer
                onClick={() => {
                  Cookies.set('mkjs_group_id', '.segment.com/', {
                    sameSite: 'none',
                    secure: true,
                    expires: coockieExpireDate,
                  });
                  setShowCookie(false);
                }}
              />
            )}

            {showGetApp && (
              <GetAppForm
                onClose={() => {
                  setShowGetApp(false);
                }}
              />
            )}

            <IntercomBtn id="intercom-btn">
              <Icon id="intercom" color={theme.colors.grey9} />
            </IntercomBtn>
            <Header
              onClick={() => {
                isMobile ? GetAppMobile() : setShowGetApp(true);
              }}
            />
            <Component {...pageProps} />
            <Footer />
          </Contet>
        </LiveChatLoaderProvider>
      </ThemeProvider>
    </>
  );
}

export default appWithTranslation(MyApp);

export const Contet = styled.div`
  @media (min-width: 1500px) {
    width: 1400px;
  }
`;

export const TopBlure = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 75px;
  filter: blur(10);
  z-index: 20;

  @media (min-width: 500px) {
    background: linear-gradient(
      180deg,
      rgb(243, 243, 243, 1) 0%,
      rgba(255, 255, 255, 0) 100%
    );
  }
`;

export const IntercomBtn = styled.div`
  position: fixed;
  right: 70px;
  bottom: 5px;
  z-index: 10;

  &:hover {
    cursor: pointer;
  }

  @media (max-width: 900px) {
    right: 5px;
    z-index: 50;
  }

  @media (min-width: 1500px) {
    right: 15px;
  }
`;
