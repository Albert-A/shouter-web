// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
const twillio = require("twilio");
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== "POST") {
    res.status(500).json({ message: "We accept only POST requests." });
  }

  const client = twillio(accountSid, authToken);
  const body = JSON.parse(req.body);

  client.messages
    .create({
      to: `+45${body.phoneNr}`,
      from: "Shouter",
      body: "Welcome to Shouter👋 \nGet our app via the link below👇 \nhttp://shouter.app/dl",
    })
    .then(
      (message: String) => res.status(200).json(message),
      (err: Error) => res.status(400).json(err)
    );
}
