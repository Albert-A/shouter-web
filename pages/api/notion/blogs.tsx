// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { Client } from '@notionhq/client';
import { Blog } from '../../../models/blog.model.shared';
import { Author } from '../../../models/author.model.shared';

const notion = new Client({ auth: process.env.NOTION_AUTH_TOKEN });
const databaseId = process.env.NOTION_DATABASE_ID || ' ';

var blogs: Blog[] = [];

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  try {
    const response = await notion.databases.query({
      database_id: databaseId,
    });

    response.results.forEach((blog) => {
      const theBlog: Blog = new Blog();
      const author: Author = new Author();

      // Normalizing the Blog general information
      theBlog.id = blog.id;
      // @ts-ignore
      theBlog.title = blog.properties.Title.title[0].plain_text;
      // @ts-ignore
      theBlog.subtitle = blog.properties.Subtitle.rich_text[0].plain_text;
      // @ts-ignore
      theBlog.cover = blog.cover.external.url;
      // @ts-ignore
      theBlog.readingTime = blog.properties.ReadTime.number;
      // @ts-ignore
      theBlog.date = new Date(blog.properties.Date.date.start);

      // Normalizing the Author
      author.id = blog.properties.Author.id;
      // @ts-ignore
      author.name = blog.properties.Author.people[0].name;
      // @ts-ignore
      author.avatar = blog.properties.Author.people[0].avatar_url;
      // @ts-ignore
      author.email = blog.properties.Author.people[0].person.email;

      theBlog.author = author;

      blogs.push(theBlog);
    });

    res.status(200).json(blogs);
    blogs = [];
  } catch (error: any) {
    console.error(error.body);
  }
}
