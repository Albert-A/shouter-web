// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { Client } from '@notionhq/client';

const notion = new Client({ auth: process.env.NOTION_AUTH_TOKEN });
const databaseId = process.env.NOTION_DATABASE_ID;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // if (req.method !== "POST") {
  //   res.status(500).json({ message: "We accept only POST requests." });
  // }

  const payload = {
    path: `/databases/${databaseId}/query`,
    method: 'POST',
  };

  try {
    // const response = await notion.pages.retrieve({
    //   page_id: '83d3908671da4a58a695981347521b63',
    // });

    const response = await notion.blocks.children.list({
      block_id: '83d3908671da4a58a695981347521b63',
      page_size: 50,
    });

    // console.log(response.results[0].heading_1.text);
    // res.status(200).json(response.results[0].heading_1.text[0].plain_text);
    res.status(200).json(response.results);
    response.results.forEach((object) => {
      console.log('TYPE: ', object.type);
    });
  } catch (error: any) {
    console.error(error.body);
  }
}
