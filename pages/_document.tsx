import Document, { Html, Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';

class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App: any) => (props: any) =>
            sheet.collectStyles(
              <>
                <App {...props} />
              </>,
            ),
        });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    const { styles } = this.props;
    return (
      <Html>
        <Head>
          <meta name="format-detection" content="telephone=no" />
          <meta
            name="description"
            content="Find tonsvis af dygtige mennesker til dine opgaver i eller udenfor hjemmet. Alt fra handyhjælp til hundeluftning - bare shout."
          />
          <meta name="author" content="Shouter®" />
          <meta name="copyright" content="© 2021 Shouter ApS" />
          <meta
            property="og:title"
            content="Få fixet dine to do's eller tjen penge på dine talenter"
          />
          <meta property="og:type" content="website" />
          <meta
            property="og:description"
            content="Find tonsvis af dygtige mennesker til dine opgaver i eller udenfor hjemmet. Alt fra handyhjælp til hundeluftning - bare shout."
          />
          <meta property="og:url" content="https://shouter.app/" />
          <meta property="og:site_name" content="Shouter®" />
          {styles}
          <link
            rel="preload"
            href="/assets/fonts/UniformRegular-Regular.woff"
            as="font"
            crossOrigin=""
          />
          <link
            rel="preload"
            href="/assets/fonts/UniformRegular-Regular.woff2"
            as="font"
            crossOrigin=""
          />
          <link
            rel="preload"
            href="/assets/fonts/UniformRnd-Ultra.woff"
            as="font"
            crossOrigin=""
          />
          <link
            rel="preload"
            href="/assets/fonts/UniformRnd-Ultra.woff2"
            as="font"
            crossOrigin=""
          />
          <link
            rel="preload"
            href="/assets/fonts/Uniform-Bold.woff"
            as="font"
            crossOrigin=""
          />
          <link
            rel="preload"
            href="/assets/fonts/Uniform-Bold.woff2"
            as="font"
            crossOrigin=""
          />

          {/* <script
            dangerouslySetInnerHTML={{
              __html: `
              !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};analytics._writeKey="C7CuANI8I5HW0OdYipNe33CUP4CEaP7g";;analytics.SNIPPET_VERSION="4.15.3";
                analytics.load("${process.env.SEGMENT_WRITE_KEY}");
                analytics.page();
              }}();

              window.intercomSettings = {
                app_id: "r4vt8zfb",
                custom_launcher_selector:'#intercom-btn',
              };

              (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/r4vt8zfb';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(document.readyState==='complete'){l();}else if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
              `,
            }}
          ></script> */}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
