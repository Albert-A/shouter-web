import { useEffect } from "react";
import useMobileDetect from "../lib/use-mobile-detect";

export default function dl() {
  const currentDevice = useMobileDetect();

  useEffect(() => {
    if (currentDevice.isIos() === true) {
      window.open(
        "itms-apps://apps.apple.com/dk/app/shouter-find-work-get-help/id1581976557",
        "_self"
      );
    } else {
      window.open(
        "https://apps.apple.com/dk/app/shouter-find-work-get-help/id1581976557",
        "_self"
      );
    }
  }, []);

  return <></>;
}
