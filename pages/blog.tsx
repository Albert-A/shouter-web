import React, { useEffect, useState } from 'react';
import { NextPage } from 'next';
import styled from 'styled-components';
import Blog from '../components/Blog/Blog';
import Lottie from '../components/Lottie/Lottie';
import { Blog as BlogModel } from '../models/blog.model.shared';

interface ErrorProps {
  statusCode: number;
}

export async function getStaticProps() {
  const response = await fetch(`https://shouter.vercel.app/api/notion/blogs`);
  const data = await response.json();

  console.log('ZDRASTI ', data);

  if (!data) {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    };
  }

  return {
    props: { data }, // will be passed to the page component as props
  };
}

const Blogs: NextPage<ErrorProps> = ({ data }: any) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [blogs, setBlogs] = useState<BlogModel[]>(data);

  return (
    <Contaier>
      <BlogContaier>
        {blogs.length != 0 && (
          <>
            {blogs?.map((blog, index) => (
              <Blog key={index} blog={blog} />
            ))}
          </>
        )}
      </BlogContaier>
    </Contaier>
  );
};

export default Blogs;

export const Contaier = styled.main`
  overflow: hidden;
  min-height: 90vh;
  padding: 0 120px;
  margin-top: 50px;
  margin-bottom: 150px;
`;

export const BlogContaier = styled.main`
  padding: 20px 0;
  display: grid;
  grid-template-columns: 45% 45%;
  grid-template-rows: auto;
  grid-gap: 50px;
  justify-content: center;
  align-items: center;
`;
