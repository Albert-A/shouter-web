module.exports = {
  locales: ['en', 'da', 'sv'],
  // An array of the locales in your applications

  createOldCatalogs: false,
  // Save the \_old files

  defaultValue: '@TODO:',
  // Default value to give to empty keys
  // You may also specify a function accepting the locale, namespace, and key as arguments

  output: 'public/locales/$LOCALE/$NAMESPACE.json',
  // Supports $LOCALE and $NAMESPACE injection
  // Supports JSON (.json) and YAML (.yml) file formats
  // Where to write the locale files relative to process.cwd()

  input: ['../common/**/*.{ts,tsx}', 'pages/**/*.{ts,tsx}'],
  // An array of globs that describe where to look for source files
  // relative to the location of the configuration file
};
