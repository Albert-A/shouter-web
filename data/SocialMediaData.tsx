import Icon from '../components/Icon/Icon';

const SocialMediaData = [
  {
    title: 'facebook',
    link: 'https://www.facebook.com/shouter.dk',
    image: <Icon id="facebook" width={27} height={27} />,
  },
  {
    title: 'spotify',
    link: 'https://open.spotify.com/user/xl3pki5wwm0n7e7qqlqyvz1yi#_=_',
    image: <Icon id="spotify" width={27} height={27} />,
  },
  {
    title: 'instagram',
    link: 'https://www.instagram.com/shouter.app/',
    image: <Icon id="instagram" width={27} height={27} />,
  },
  {
    title: 'linkedin',
    link: 'https://www.linkedin.com/company/30609500/admin/',
    image: <Icon id="linkedin" width={27} height={27} />,
  },
];

export default SocialMediaData;
