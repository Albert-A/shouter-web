import Image from 'next/image';
import Icon from '../components/Icon/Icon';

const ColaboratorsData = [
  {
    title: 'sinful',
    link: 'https://www.sinful.dk/',
    image: <Icon id="sinful" />,
  },
  {
    title: 'lunar',
    link: 'https://lunar.app/dk/',
    // image: <Image src="/assets/lunar.png" alt="lunar" width={94} height={31} />,
    image: <Icon id="lunar" width={90} height={30} />,
  },
  {
    title: 'trifork',
    link: 'https://trifork.com/',
    image: <Icon id="trifork" />,
  },
  {
    title: 'D',
    link: 'https://www.designit.com/',
    // image: <Image src="/assets/D.png" alt="D" width={67} height={67} />,
    image: <Icon id="designit" />,
  },
  {
    title: 'airtame',
    link: 'https://airtame.com/',
    image: (
      <Image src="/assets/airtame.png" alt="airtame" width={158} height={28} />
    ),
  },
  {
    title: 'be my eyes',
    link: 'https://www.bemyeyes.com/',
    // image: <Image src="/assets/eyes.png" alt="eyes" width={115} height={51} />,
    image: <Icon id="bemyeyes" />,
  },
  {
    title: 'cuture',
    link: 'https://cultureworks.dk/',
    image: (
      <Image src="/assets/cuture.png" alt="cuture" width={80} height={34} />
    ),
  },
  {
    title: 'incommodities',
    link: 'https://incommodities.com/',
    image: (
      <Image
        src="/assets/incommodities.png"
        alt="incommodities"
        width={78}
        height={56}
      />
    ),
  },
  {
    title: 'A',
    link: 'https://www.artboost.com/?no_redirect=true',
    image: <Image src="/assets/A.png" alt="A" width={42} height={42} />,
  },
];

export default ColaboratorsData;
