import React from "react";
import Icon from "../Icon/Icon";
import { ITheme } from "../../lib/theme";
import styled from "styled-components";

export interface ButtonProps {
  children?: React.ReactNode;
  size?: "xxs" | "xs" | "s" | "m" | "l" | "xl" | "xl" | "xxl";
  color?: keyof ITheme["colors"];
  hover?: boolean;
  onClick: (e?: any) => void;
  disabled?: boolean;
  naked?: boolean;
  icon?: string;
  iconColor?: string;
  iconSize?: number;
  asLink?: boolean;
  disabledBackground?: string;
  disabledIconColor?: string;
}

export const Button: React.FC<ButtonProps> = ({
  children,
  size,
  color = "purpleStrong",
  hover = false,
  disabled = false,
  onClick,
  naked = false,
  icon,
  iconColor,
  iconSize = 24,
  asLink = false,
  disabledBackground,
  disabledIconColor,
}: ButtonProps) => {
  return (
    <ButtonContainer
      disabled={disabled}
      hover={hover}
      color={color}
      size={size}
      naked={naked}
      hasChildren={children}
      asLink={asLink}
      disabledBackground={disabledBackground}
      onClick={onClick}
    >
      {icon && (
        <Icon
          color={disabled ? disabledIconColor || null : iconColor || null}
          id={icon}
          width={iconSize}
          height={iconSize}
        />
      )}
      {children && typeof children === "string" && (
        <ButtonLabel
          disabled={disabled}
          icon={icon}
          color={color}
          size={size}
          naked={naked}
        >
          {children}
        </ButtonLabel>
      )}
      {asLink && (
        <ArrowIconContainer>
          <Icon
            color={iconColor || null}
            id="rightArrow"
            width={iconSize}
            height={iconSize}
          />
        </ArrowIconContainer>
      )}
      {children && typeof children !== "string" ? (
        <ContentContainer icon={!!icon}>{children}</ContentContainer>
      ) : (
        <></>
      )}
    </ButtonContainer>
  );
};

const ButtonContainer = styled.button<{
  size?: string;
  color: keyof ITheme["colors"];
  hover?: boolean;
  disabled?: boolean;
  naked?: boolean;
  hasChildren?: any;
  asLink?: boolean;
  disabledBackground?: string;
}>`
  display: flex;
  flex-direction: row;
  justify-content: ${({ asLink }) => (asLink ? "flex-start" : "center")};
  align-items: center;
  height: ${({ size }) =>
    size === "xxs"
      ? 25
      : size === "xs"
      ? 40
      : size === "s"
      ? 48
      : size === "m"
      ? 60
      : size === "xl"
      ? 100
      : size === "xxl"
      ? 120
      : 70}px;

  padding: 0
    ${({ theme, size }) =>
      size === "xs" || size === "xxs"
        ? theme.spacings.small
        : theme.spacings.large}px;
  border-radius: ${({ size }) =>
    size === "xxs" ? 8 : size === "xs" ? 10 : 20}px;
  border: 0px solid ${({ theme }) => theme.colors.black};
  border-width: ${({ naked }) => (naked ? "1px" : "0px")};
  background: ${({ color, theme }) => theme.colors[color] ?? "white"};

  ${({ hover }) =>
    hover
      ? `shadow-color: #000;
    shadow-offset: 0 3px;
    shadow-opacity: 0.10;
    shadow-radius: 10px;
    elevation: 5;`
      : ""}

  ${({ disabled, disabledBackground, theme }) =>
    disabled && !disabledBackground
      ? `background: ${theme.colors.grey2}`
      : disabledBackground
      ? `background: ${disabledBackground}`
      : ""};

  ${({ naked }) => (naked ? "background: #00000000" : "")};
`;

const ContentContainer = styled.div<{ icon?: boolean }>`
  display: flex;
  flex-direction: column;
  flex: 1;
  ${({ icon, theme }) =>
    icon ? `margin-left: ${theme.spacings.medium}px` : ""};
`;

const ArrowIconContainer = styled.div`
  position: absolute;
  right: ${({ theme }) => theme.spacings.small}px;
`;

const ButtonLabel = styled.div<{
  size?: string;
  color: keyof ITheme["colors"];
  icon: any;
  disabled?: boolean;
  naked?: boolean;
}>`
  font-size: ${({ theme, size }) =>
    !size ? theme.fontSizes.normal : theme.fontSizes.normal}px;
  color: ${({ color, theme }) => color};
  font-family: ${({ theme }) => theme.fontFamilies.regular};
  text-align: center;
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};

  ${({ icon }) => (icon ? "margin-left: 10px" : "")};
`;
