import * as React from "react";
import { useState } from "react";
import styled from "styled-components";
import { DefaultTheme } from "styled-components";

interface ToggleProps {
  theme?: DefaultTheme;
  onClick: (e?: any) => void;
}

const Toggle: React.FC<ToggleProps> = ({ theme, onClick }: ToggleProps) => {
  const [isShouter, setIsShouter] = useState<boolean>(true);

  const trackToggleClicked = () => {
    // @ts-ignore window.analytics undefined below
    global.analytics.track("Switch button pressed", {
      site_state: isShouter ? "Shouter" : "Doer",
    });
  };

  return (
    <ToggleContaier
      isShouter={isShouter}
      onClick={() => {
        onClick();
        setIsShouter(!isShouter);
        trackToggleClicked();
      }}
    >
      <Swiper isShouter={isShouter} />
    </ToggleContaier>
  );
};

export default Toggle;

export const ToggleContaier = styled.button<{ isShouter: boolean }>`
  width: 165px;
  height: 90px;
  background: ${({ theme }) => theme.colors.grey9};
  border-radius: 30px;
  padding: 14px 12px;
  border: 0;
  display: flex;
  align-items: center;
  justify-content: ${({ isShouter }) =>
    isShouter ? "flex-end" : "flex-start"};
  -webkit-tap-highlight-color: transparent;

  &:hover {
    cursor: pointer;
  }

  @media (max-width: 1100px) {
    margin-top: 60px;
    width: 120px;
    height: 65px;
    border-radius: 23px;
  }
`;

export const Swiper = styled.div<{ isShouter: boolean }>`
  position: absolute;
  width: 70px;
  height: 65px;
  margin: 0 3px;
  background: ${({ isShouter }) =>
    isShouter
      ? "linear-gradient(90deg, #ffcae9 0.68%, #ff9cd7 96.93%)"
      : "linear-gradient(270deg, #F5B388 -0.21%, #F8995C 99.6%);"};
  border-radius: 20px;
  border: 0;

  @media (max-width: 1100px) {
    margin: 0;
    width: 50px;
    height: 47px;
    border-radius: 17px;
  }
`;
