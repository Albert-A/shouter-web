import Link from 'next/dist/client/link';
import { useEffect, useState } from 'react';
import styled from 'styled-components';
import SocialMediaData from '../../data/SocialMediaData';
import theme from '../../lib/theme';
import useMobileDetect from '../../lib/use-mobile-detect';
import Typography from '../Typography/Typography';

const Footer: React.FC = () => {
  const currentDevice = useMobileDetect();

  const [isMobile, setisMobile] = useState<boolean>(false);

  useEffect(() => {
    setisMobile(currentDevice.isMobile());
  }, [currentDevice]);

  // useEffect(() => {
  //   // @ts-ignore window.analytics undefined below
  //   global.analytics.identify({
  //     userStatus: 'undefined',
  //   });
  // }, []);

  return (
    <FooterContier>
      <SocialMediaLinks>
        {SocialMediaData.map((value, index) => (
          <Link href={value.link} key={index}>
            <a target="_blank" rel="noreferrer">
              <SocialItem>{value.image}</SocialItem>
            </a>
          </Link>
        ))}
      </SocialMediaLinks>
      <MadeWithLove>
        <Typography align="center" size={10} color={theme.colors.grey4}>
          {'Made with 🤙 by the Shouter® '}
        </Typography>
        <Typography align="center" size={10} color={theme.colors.grey4}>
          {' © 2020 All rights reseved'}
        </Typography>
      </MadeWithLove>
      <Address>
        <div style={{ maxWidth: 130 }}>
          <Typography align="right" size={10} color={theme.colors.grey4}>
            Marselisborg Havnevej 30 8000 Aarhus C, Denmark CVR: 40020179
          </Typography>
        </div>
      </Address>
    </FooterContier>
  );
};

export default Footer;

export const FooterContier = styled.footer`
  width: 100%;
  height: 120px;
  display: flex;
  justify-content: flex-start;
  align-items: space-between;
  padding: 0 150px;

  @media (max-width: 700px) {
    justify-content: center;
    flex-direction: column;
    padding: 0 25px;
  }
`;

export const SocialMediaLinks = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;

  @media (max-width: 700px) {
    align-items: center;
    justify-content: center;
  }
`;

export const SocialItem = styled.div`
  margin: 0 10px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;

export const MadeWithLove = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  @media (max-width: 700px) {
    margin-top: 25px;
  }
`;

export const Address = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;

  @media (max-width: 700px) {
    display: none;
  }
`;
