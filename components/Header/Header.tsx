import { useTranslation } from "next-i18next";
import Link from "next/dist/client/link";
import * as React from "react";
import { useEffect, useState } from "react";
import styled from "styled-components";
import useMobileDetect from "../../lib/use-mobile-detect";
import Icon from "../Icon/Icon";
import Typography from "../Typography/Typography";

interface HeaderProps {
  onClick: (e?: any) => void;
}

const Header: React.FC<HeaderProps> = ({ onClick }: HeaderProps) => {
  // Hooks
  const { t } = useTranslation();
  const currentDevice = useMobileDetect();

  const [isMobile, setisMobile] = useState<boolean>(false);

  useEffect(() => {
    setisMobile(currentDevice.isMobile());
  }, [currentDevice]);

  const trackJoinUs = () => {
    // @ts-ignore window.analytics undefined below
    global.analytics.track("App store link clicked", {
      message: "Join us clikced",
    });
  };

  return (
    <HeaderContaier>
      <Link href="/">
        <LogoContier>
          <Icon id="logo" />
        </LogoContier>
      </Link>

      <Button
        onClick={() => {
          onClick();
          trackJoinUs();
        }}
      >
        <GetApp>
          <Typography color="white" size={12} font="bold">
            {t("translation:getApp")}
          </Typography>

          {!isMobile && (
            <IconContainer>
              <Icon id="arrowUp" width={10} height={10} />
            </IconContainer>
          )}
        </GetApp>
      </Button>
    </HeaderContaier>
  );
};

export default Header;

export const HeaderContaier = styled.header`
  position: sticky;
  top: 0px;
  padding-top: ${({ theme }) => theme.spacings.large}px;
  padding-bottom: ${({ theme }) => theme.spacings.small}px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  z-index: 50;

  @media (max-width: 900px) {
    background: linear-gradient(
      180deg,
      rgba(255, 255, 255, 1) 0%,
      rgba(255, 255, 255, 0) 100%
    );
  }
`;

export const LogoContier = styled.div`
  width: 110px;
  height: ${({ theme }) => theme.spacings.large}px;
  margin-left: 90px;
  transition: 0.2s;

  &:hover {
    cursor: pointer;
    transform: scale(1.05);
  }

  @media (max-width: 900px) {
    margin-left: 25px;
  }
`;

export const Button = styled.button`
  width: 120px;
  height: 45px;
  margin-right: 90px;
  border: 0px;
  background: transparent;
  transition: 0.2s;

  &:hover {
    cursor: pointer;
    transform: scale(1.05);
  }

  @media (max-width: 900px) {
    margin-right: 25px;
  }

  @media (max-width: 700px) {
    width: 91px;
    height: 35px;
  }
`;

export const GetApp = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  margin-right: 35px;
  border-radius: 30px;
  background: ${({ theme }) => theme.colors.grey9};
  box-shadow: 0px 10px 35px -15px rgba(0, 0, 0, 0.45);

  @media (max-width: 700px) {
    border-radius: 10px;
  }
`;

export const IconContainer = styled.div`
  margin-left: 5px;
`;
