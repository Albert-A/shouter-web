import styled from 'styled-components';
import Typography from '../Typography/Typography';
import { useTranslation } from 'next-i18next';
import React, { useEffect, useState } from 'react';
import Icon from '../Icon/Icon';
import theme from '../../lib/theme';
import Lottie from '../Lottie/Lottie';

export interface GetAppProps {
  onClose: (e?: any) => void;
}

export const GetAppForm: React.FC<GetAppProps> = ({ onClose }: GetAppProps) => {
  const { t } = useTranslation();

  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [phone, setPhone] = useState<string | undefined>('');
  const [phoneToShow, setPhoneToShow] = useState<string | undefined>('');
  const [opacity, setOpacity] = useState<number>(0);
  const [showButton, setShowButton] = useState<boolean>(true);
  const [thanksMessage, setShowThanksMessage] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>('');

  useEffect(() => {
    setOpacity(1);
  }, []);

  const trackPhoneNumber = () => {
    // @ts-ignore window.analytics undefined below
    global.analytics.track('Lead generator used', {
      phone: `+45${phone}`,
    });
  };

  const validate = (value: string) => {
    if (value.length < 12) {
      value = value.split(' ').join(''); // Remove dash (-) if mistakenly entered.
      let finalVal = value.match(/.{1,2}/g)?.join(' ');

      if (value === '') {
        setPhoneToShow('');
        setPhone('');
        setShowButton(true);
      } else {
        if (value.match(/^[0-9 ]+$/)) {
          value.length >= 8 ? setShowButton(false) : setShowButton(true);

          // Clear error message
          setErrorMessage('');
          setError(false);

          setPhoneToShow(finalVal);
          finalVal = finalVal?.split(' ').join('');
          setPhone(finalVal);

          return true;
        } else {
          setErrorMessage('You can type only numbers!');
          setError(true);
          return false;
        }
      }
    }
  };

  const sendMessage = async () => {
    if (validate(phone ? phone : '')) {
      setLoading(true);
      const response = await fetch(`/api/twilio`, {
        method: 'POST',
        body: JSON.stringify({ phoneNr: phone }),
      }).then((response) => {
        if (response.status === 200) {
          setLoading(false);
          setShowThanksMessage(true);

          setTimeout(() => {
            setOpacity(0);
          }, 1000);
          setTimeout(() => {
            onClose();
          }, 1150);
        } else {
          setErrorMessage(
            "We could't send you the message. Check the phone number and try again.",
          );
          setError(true);
        }
      });
    }
  };

  return (
    <Container style={{ opacity: opacity }}>
      <CloseButton
        onClick={() => {
          setOpacity(0);
          setTimeout(() => onClose(), 400);
        }}
      >
        <Icon id="crossCircle" />
      </CloseButton>

      {opacity === 1 && (
        <>
          {!thanksMessage ? (
            <>
              {loading ? (
                <>
                  <Lottie id="plane" width={500} height={300} />
                </>
              ) : (
                <>
                  <Typography font="bold" size={50}>
                    Sign up for Shouter
                  </Typography>

                  <div style={{ maxWidth: 400 }}>
                    <Typography spacingBefore={theme.spacings.xlarge}>
                      Get an SMS with a link to the app, try us by downloading
                      the app.
                    </Typography>
                  </div>

                  <DownloadLinkContainer>
                    <InputContaier isEmpty={showButton}>
                      <Typography size={16}>+45</Typography>
                      <Input
                        autoFocus
                        type="tel"
                        id="phone"
                        name="phone"
                        placeholder="Enter Number"
                        value={phoneToShow}
                        onInput={(event) => validate(event.currentTarget.value)}
                        onKeyUp={(event) => {
                          validate(event.currentTarget.value);
                        }}
                      />
                    </InputContaier>

                    <SignUpBtm
                      isEmpty={showButton}
                      onClick={() => {
                        sendMessage();
                        trackPhoneNumber();
                      }}
                    >
                      {!showButton && (
                        <Typography color={theme.colors.white}>
                          Sign Up
                        </Typography>
                      )}
                    </SignUpBtm>
                  </DownloadLinkContainer>

                  {error && (
                    <div style={{ maxWidth: 400 }}>
                      <Typography
                        spacingBefore={20}
                        align="center"
                        font="bold"
                        color="#fc1c1c"
                      >
                        {errorMessage}
                      </Typography>
                    </div>
                  )}
                </>
              )}
            </>
          ) : (
            <>
              <Typography font="bold" size={50}>
                Thanks for using Shouter
              </Typography>
            </>
          )}
        </>
      )}
    </Container>
  );
};

export const Container = styled.div`
  top: 0px;
  left: 0px;
  position: fixed;
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: rgba(255, 255, 255, 0.85);
  z-index: 100;
  backdrop-filter: blur(5px);
  transition: 0.5s;

  @media (max-width: 500px) {
    padding: 0 40px;
  }
`;

export const CloseButton = styled.button`
  position: absolute;
  top: 20px;
  right: 20px;
  transition: 0.2s;

  &:hover {
    transform: scale(1.05);
  }
`;

export const DownloadLinkContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 55px;
  margin-top: ${({ theme }) => theme.spacings.xxlarge}px;

  @media (max-width: 500px) {
    flex-direction: column;
    height: 95px;
  }
`;

export const InputContaier = styled.div<{ isEmpty: boolean }>`
  height: 55px;
  width: ${({ isEmpty }) => (isEmpty ? 300 : 200)}px;
  background: ${({ theme }) => theme.colors.white};
  border-radius: 50px;
  box-shadow: rgb(0 0 0 / 15%) 0px 0px 20px;
  padding: 5px 20px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  transition: 0.5s;
`;

export const Input = styled.input`
  border: 0px;
  margin-left: 10px;
  width: 130px;
  color: ${({ theme }) => theme.colors.grey7};
  -webkit-tap-highlight-color: transparent;
  outline: none;
`;

export const SignUpBtm = styled.button<{ isEmpty: boolean }>`
  width: ${({ isEmpty }) => (isEmpty ? 0 : 130)}px;
  height: 100%;
  background: ${({ theme }) => theme.colors.grey9};
  -webkit-tap-highlight-color: transparent;
  outline: none;
  border-radius: 50px;
  margin-left: ${({ isEmpty }) => (isEmpty ? 0 : 20)}px;
  transition: 0.7s;

  @media (max-width: 500px) {
    flex-direction: column;
    margin-left: 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 10px;
    height: 55px;
  }

  &:hover {
    transition: 0.2s;
    transform: scale(1.05);
  }
`;
