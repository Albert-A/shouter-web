import * as React from "react";
import styled from "styled-components";

interface TypographyProps {
  color?: string;
  align?: "center" | "left" | "right";
  children?: React.ReactNode;
  onClick?: () => void;
  font?: "ultra" | "regular" | "bold";
  size?: number;
  lineHeight?: number;
  displayBlock?: boolean;
  spacingBefore?: number;
  spacingAfter?: number;
}

const Typography: React.FC<TypographyProps> = ({
  color,
  align = "center",
  children,
  font = "regular",
  size,
  lineHeight,
  spacingBefore,
  spacingAfter,
  displayBlock,
  onClick,
}: TypographyProps): JSX.Element => (
  <TextContainer
    spacingBefore={spacingBefore}
    spacingAfter={spacingAfter}
    displayBlock={displayBlock}
    font={font}
    align={align}
    size={size}
    color={color}
    lineHeight={lineHeight}
  >
    {children}
  </TextContainer>
);

export default Typography;

export const TextContainer = styled.p<{
  type?: string;
  color?: string;
  align?: string;
  font?: string;
  fontMobile?: string;
  size?: number | null;
  sizeMobile?: number | null;
  lineHeight?: number;
  displayBlock?: boolean;
  spacingBefore?: number;
  spacingAfter?: number;
  spacingBeforeMobile?: number;
}>`
  font-family: ${({ font, theme }) =>
    font === "ultra"
      ? theme.fontFamilies.ultra
      : font === "bold"
      ? theme.fontFamilies.bold
      : "Uniform"};
  font-size: ${({ size, theme }) => (size ? size : theme.fontSizes.normal)}px;
  font-weight: ${({ type }) => (type === "title" ? "bold" : "normal")};
  color: ${({ color, theme }) => (color ? color : theme.colors.black)};
  display: ${({ displayBlock }) => (displayBlock ? "block" : "flex")};
  text-align: ${({ align }) => (align ? align : "left")};
  justify-content: ${({ align }) =>
    align === "left"
      ? "flex-start"
      : align === "center"
      ? "center"
      : "flex-end"};
  ${({ lineHeight }) => (lineHeight ? `line-height: ${lineHeight}px;` : "")}
  margin-top: ${({ spacingBefore }) => spacingBefore || 0}px;
  margin-bottom: ${({ spacingAfter }) => spacingAfter || 0}px;
`;
