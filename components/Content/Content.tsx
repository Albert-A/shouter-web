import Link from "next/dist/client/link";
import { useEffect, useState } from "react";
import styled from "styled-components";
import ColaboratorsData from "../../data/ColaboratorsData";
import useMobileDetect from "../../lib/use-mobile-detect";
import Typography from "../Typography/Typography";
import Image from "next/image";
import theme from "../../lib/theme";
import Icon from "../Icon/Icon";
import { useTranslation } from "next-i18next";
import Lottie from "../Lottie/Lottie";

interface ContentProps {
  isShouter: boolean;
}

export const Content: React.FC<ContentProps> = ({
  isShouter,
}: ContentProps) => {
  // Hooks
  const { t } = useTranslation("translation");
  const currentDevice = useMobileDetect();

  const [isMobile, setisMobile] = useState<boolean>(false);

  useEffect(() => {
    setisMobile(currentDevice.isMobile());
  }, [currentDevice]);

  const GetAppMobile = () => {
    if (currentDevice.isIos() === true) {
      window.open(
        "itms-apps://apps.apple.com/dk/app/shouter-find-work-get-help/id1581976557",
        "_blank"
      );
    } else {
      window.open(
        "https://apps.apple.com/dk/app/shouter-find-work-get-help/id1581976557",
        "_blank"
      );
    }
  };

  return (
    <>
      <div style={{ maxWidth: isShouter ? 380 : 260 }}>
        <Typography
          spacingBefore={isMobile ? 110 : 150}
          align="center"
          size={isMobile ? theme.fontSizes.header : theme.fontSizes.display}
          font="bold"
        >
          {isShouter
            ? t("translation:beShouterText")
            : t("translation:beDoerText")}
        </Typography>
      </div>

      <InstructionContainer>
        <Instruction style={{ marginTop: isMobile ? 60 : 80 }}>
          <InstructionContent>
            <InstructionText>
              <Typography
                align="left"
                size={theme.fontSizes.display}
                font={isMobile ? "bold" : "ultra"}
                color={theme.colors.grey4}
              >
                01
              </Typography>
              <Typography
                align="left"
                size={
                  isMobile ? theme.fontSizes.header : theme.fontSizes.display
                }
                font={isMobile ? "bold" : "ultra"}
              >
                {isShouter
                  ? t("translation:firstStepTitleShouter")
                  : t("translation:firstStepTitleDoer")}
              </Typography>
              <Typography
                align="left"
                size={theme.fontSizes.medium}
                spacingBefore={theme.spacings.large}
              >
                {isShouter
                  ? t("translation:firstStepShouter")
                  : t("translation:firstStepDoer")}
              </Typography>
            </InstructionText>
            <InstructionImage style={{ marginLeft: 70, right: 0 }}>
              <Lottie
                id={isShouter ? "text" : "map"}
                width={isMobile ? 180 : 230}
                height={isMobile ? 300 : 450}
              />
            </InstructionImage>
          </InstructionContent>
          <InstructionIcon>
            <div style={{ marginLeft: 80 }}>
              <Icon id="mouth1" />
            </div>
          </InstructionIcon>
        </Instruction>

        <Instruction style={{ marginTop: isMobile ? 0 : 0 }}>
          <InstructionIcon>
            <div style={{ marginRight: 80 }}>
              <Icon id="mouth2" />
            </div>
          </InstructionIcon>
          <InstructionContent style={{ justifyContent: "flex-end" }}>
            <InstructionImage deg={10} style={{ marginRight: 70, left: -40 }}>
              <Lottie
                id={isShouter ? "profile" : "task"}
                width={isMobile ? 180 : 230}
                height={isMobile ? 300 : 450}
              />
            </InstructionImage>
            <InstructionText style={{ marginLeft: 40 }}>
              <Typography
                align="left"
                size={theme.fontSizes.display}
                font={isMobile ? "bold" : "ultra"}
                color={theme.colors.grey4}
              >
                02
              </Typography>
              <InTitle>
                <Typography
                  align="left"
                  size={
                    isMobile ? theme.fontSizes.header : theme.fontSizes.display
                  }
                  font={isMobile ? "bold" : "ultra"}
                >
                  {isShouter
                    ? t("translation:stepTwoTitleShouter")
                    : t("translation:stepTwoTitleDoer")}
                </Typography>
              </InTitle>

              <Typography
                align="left"
                size={theme.fontSizes.medium}
                spacingBefore={theme.spacings.large}
              >
                {isShouter
                  ? t("translation:stepTwoShouter")
                  : t("translation:stepTwoDoer")}
              </Typography>
            </InstructionText>
          </InstructionContent>
        </Instruction>

        <Instruction style={{ marginTop: isMobile ? 0 : 0 }}>
          <InstructionContent>
            <InstructionText>
              <Typography
                align="left"
                size={theme.fontSizes.display}
                font={isMobile ? "bold" : "ultra"}
                color={theme.colors.grey4}
              >
                03
              </Typography>
              <InTitle>
                <Typography
                  align="left"
                  size={
                    isMobile ? theme.fontSizes.header : theme.fontSizes.display
                  }
                  font={isMobile ? "bold" : "ultra"}
                >
                  {isShouter
                    ? t("translation:stepThreeTitleShouter")
                    : t("translation:stepThreeTitleDoer")}
                </Typography>
              </InTitle>
              <Typography
                align="left"
                size={theme.fontSizes.medium}
                spacingBefore={theme.spacings.large}
              >
                {isShouter
                  ? t("translation:stepThreeShouter")
                  : t("translation:stepThreeDoer")}
              </Typography>
            </InstructionText>
            <InstructionImage style={{ marginLeft: 70, right: 0 }}>
              <Lottie
                id={isShouter ? "chat" : "wallet"}
                width={isMobile ? 180 : 230}
                height={isMobile ? 300 : 450}
              />
            </InstructionImage>
          </InstructionContent>
          <InstructionIcon>
            <div style={{ marginLeft: 80 }}>
              <Icon id="mouth3" />
            </div>
          </InstructionIcon>
        </Instruction>
      </InstructionContainer>

      {isMobile && (
        <GetApp onClick={GetAppMobile}>
          <Typography color="white" size={12} font="bold">
            Get app
          </Typography>
        </GetApp>
      )}

      <div style={{ maxWidth: 440 }}>
        <Typography
          spacingBefore={isMobile ? 95 : 150}
          font="bold"
          size={isMobile ? 30 : 45}
        >
          {isShouter
            ? t("translation:findTextShouter")
            : t("translation:findTextDoer")}
        </Typography>
      </div>

      <BottomImage>
        <Image
          src={isShouter ? "/assets/Shouters.png" : "/assets/Doers.png"}
          alt="shouter/doer"
          width={isShouter ? 3831 : 3818}
          height={isShouter ? 2975 : 2964}
          unoptimized={true}
          loading="eager"
        />
      </BottomImage>

      {isMobile && (
        <button onClick={GetAppMobile}>
          <AppStore>
            <Icon id="appStore" />
          </AppStore>
        </button>
      )}

      <div style={{ maxWidth: 440 }}>
        <Typography
          spacingBefore={isMobile ? 95 : 150}
          font="bold"
          size={isMobile ? 30 : 45}
        >
          {t("translation:trusted")}
        </Typography>
      </div>

      <ColaboratioContaier>
        {ColaboratorsData.map((value, index) => (
          <Link href={value.link} key={index}>
            <a target="_blank" rel="noreferrer">
              <Colaborator>{value.image}</Colaborator>
            </a>
          </Link>
        ))}
      </ColaboratioContaier>
    </>
  );
};

export const VerticalSeparator = styled.div`
  height: 100%;
  width: 1px;
  margin-left: ${({ theme }) => theme.spacings.large}px;
  margin-right: ${({ theme }) => theme.spacings.large}px;
  background: ${({ theme }) => theme.colors.grey5};
`;

export const AppStore = styled.div`
  margin-top: ${({ theme }) => theme.spacings.xlarge}px;
`;

export const ModeText = styled.div`
  max-width: 430px;
`;

export const ModeTextMobile = styled.div`
  display: flex;
  max-width: 260px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const ColaboratioContaier = styled.div`
  margin-top: 140px;
  max-width: 720px;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;

  @media (max-width: 500px) {
    margin-top: 80px;
  }

  -webkit-tap-highlight-color: transparent;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
`;

export const Colaborator = styled.div`
  margin: ${({ theme }) => theme.spacings.xxlarge}px
    ${({ theme }) => theme.spacings.large}px 0px
    ${({ theme }) => theme.spacings.large}px;
  display: flex;
  justify-content: center;
  align-items: center;
  -webkit-tap-highlight-color: transparent;

  &:hover {
    cursor: pointer;
  }

  @media (max-width: 700px) {
    width: 80%;
    margin: ${({ theme }) => theme.spacings.medium}px
      ${({ theme }) => theme.spacings.medium}px 0px
      ${({ theme }) => theme.spacings.medium}px;
  }
`;

export const InstructionContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const Instruction = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const InstructionContent = styled.div`
  width: 50%;
  display: flex;

  @media (max-width: 1100px) {
    position: relative;
    width: 100%;
    justify-content: space-between;
  }
`;

export const InstructionText = styled.div<{ maxWidth?: number }>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  height: 600px;

  p {
    width: 170px;
  }

  @media (max-width: 500px) {
    p {
      max-width: 150px;
    }
    height: 480px;
  }

  @media (max-width: 360px) {
    height: 100%;
    margin-top: ${({ theme }) => theme.spacings.large}px;
  }
`;

export const InTitle = styled.div`
  p {
    width: 210px;
  }

  @media (max-width: 500px) {
    p {
      max-width: 150px;
    }
  }
`;

export const InstructionImage = styled.div<{ deg?: number }>`
  border-radius: 22px;
  transform: rotate(${({ deg }) => (deg ? deg : "-10")}deg);
  max-height: 450px;
  width: 230px;
  margin-top: 50px;

  box-shadow: ${({ deg }) => (deg ? deg : "-20")}px 22px 70px 25px
    rgba(0, 0, 0, 0.1);

  @media (max-width: 900px) {
    position: absolute;
    height: 340px;
    width: 160px;
    transform: rotate(${({ deg }) => (deg ? deg * 0.6 : "-10")}deg);
    margin-top: 80px;
  }

  @media (max-width: 360px) {
    display: none;
  }
`;

export const ImageCont = styled.div<{ deg?: number }>`
  transform: rotate(${({ deg }) => (deg ? deg : "-10")}deg);
  width: 400px;

  @media (max-width: 700px) {
    position: absolute;
    width: 320px;
  }

  @media (max-width: 360px) {
    display: none;
  }
`;

export const InstructionIcon = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 50px;

  @media (min-width: 1200px) {
    width: 50%;
  }

  @media (max-width: 1100px) {
    display: none;
  }
`;

export const BottomImage = styled.div<{}>`
  margin-top: 170px;
  width: 110%;

  @media (max-width: 1100px) {
    margin-top: 80px;
    width: 900px;
  }
`;

export const GetApp = styled.button`
  border-radius: 10px;
  width: 130px;
  height: 50px;
  margin-top: 40px;
  border: 0px;
  background: ${({ theme }) => theme.colors.grey9};
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 10px 35px -15px rgba(0, 0, 0, 0.45);
`;
