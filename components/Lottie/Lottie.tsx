import { useEffect } from 'react';
import Lottie from 'react-lottie';
import styled from 'styled-components';

interface LottieProps {
  id: string;
  width?: number;
  height?: number;
}

import textLottie from '../../public/assets/lotties/TEXT.json';
import profileLottie from '../../public/assets/lotties/PROFILE.json';
import chatLottie from '../../public/assets/lotties/CHAT.json';
import mapLottie from '../../public/assets/lotties/MAP.json';
import tasktLottie from '../../public/assets/lotties/TASK.json';
import walletLottie from '../../public/assets/lotties/WALLET.json';
import messageLottie from '../../public/assets/lotties/message.json';
import planeLottie from '../../public/assets/lotties/plane.json';
import notFoundLottie from '../../public/assets/lotties/404.json';
import internalErrorLottie from '../../public/assets/lotties/500.json';

const LottieComp: React.FC<LottieProps> = ({
  id,
  width,
  height,
}: LottieProps): JSX.Element | null => {
  const data: any = {
    text: {
      content: {
        loop: true,
        autoplay: true,
        animationData: textLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 394,
      height: 832,
    },
    profile: {
      content: {
        loop: true,
        autoplay: true,
        animationData: profileLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 394,
      height: 832,
    },
    chat: {
      content: {
        loop: true,
        autoplay: true,
        animationData: chatLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 394,
      height: 832,
    },
    map: {
      content: {
        loop: true,
        autoplay: true,
        animationData: mapLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 394,
      height: 832,
    },
    task: {
      content: {
        loop: true,
        autoplay: true,
        animationData: tasktLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 394,
      height: 832,
    },
    wallet: {
      content: {
        loop: true,
        autoplay: true,
        animationData: walletLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 394,
      height: 832,
    },
    message: {
      content: {
        loop: true,
        autoplay: true,
        animationData: messageLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 400,
      height: 400,
    },
    plane: {
      content: {
        loop: true,
        autoplay: true,
        animationData: planeLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 800,
      height: 600,
    },
    notFound: {
      content: {
        loop: true,
        autoplay: true,
        animationData: notFoundLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 500,
      height: 300,
    },
    internalError: {
      content: {
        loop: true,
        autoplay: true,
        animationData: internalErrorLottie,
        rendererSettings: {
          // preserveAspectRatio: "xMidYMid slice",
        },
      },
      width: 500,
      height: 300,
    },
  };

  const lottie = data[id];
  if (!lottie) {
    return null;
  }

  return (
    <Wrapper width={width || lottie.width} height={height || lottie.height}>
      <Lottie
        options={lottie.content}
        width={lottie.width}
        height={lottie.height}
      />
    </Wrapper>
  );
};

export default LottieComp;

export const Wrapper = styled.div<{ width: number; height: number }>`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  width: ${({ width }) => width}px;
  height: ${({ height }) => height}px;
`;
