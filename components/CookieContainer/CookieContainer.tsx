import styled from "styled-components";
import theme from "../../lib/theme";
import Typography from "../Typography/Typography";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import useMobileDetect from "../../lib/use-mobile-detect";

export interface CookieContainerProps {
  onClick: (e?: any) => void;
}

export const CookieContainer: React.FC<CookieContainerProps> = ({
  onClick,
}: CookieContainerProps) => {
  // Hooks
  const { t } = useTranslation();
  const router = useRouter();
  const currentDevice = useMobileDetect();

  // States
  const [isMobile, setisMobile] = useState<boolean>(false);
  const [opacity, setOpacity] = useState<number>(0);

  useEffect(() => {
    setisMobile(currentDevice.isMobile());
  }, [currentDevice]);

  useEffect(() => {
    setTimeout(() => setOpacity(1), 400);
  }, []);

  const cookieUrl =
    router.locale === "da"
      ? "https://intercom.help/shouterapp/da/articles/5026302-privatlivs-cookiepolitik"
      : "https://intercom.help/shouterapp/en/articles/5026302-privacy-cookie-policy";

  return (
    <Container style={{ opacity: opacity }}>
      {opacity === 1 && (
        <>
          <Typography
            color={theme.colors.white}
            font="bold"
            size={isMobile ? 30 : 55}
          >
            {t("translation:cookieTitle")}
          </Typography>
          <div style={{ maxWidth: 700 }}>
            <Typography
              color={theme.colors.white}
              spacingBefore={theme.spacings.xxlarge}
              size={isMobile ? 14 : 18}
              align="center"
            >
              {t("translation:cookieTesxt")}
            </Typography>
          </div>
          <PolicyLink href={cookieUrl} rel="noopener" target="_blank">
            <Typography color={theme.colors.white} size={isMobile ? 14 : 18}>
              <u>{t("translation:cookiePolicy")}</u>
            </Typography>
          </PolicyLink>
          <AcceptBtn
            onClick={() => {
              setOpacity(0);
              setTimeout(() => onClick(), 400);
            }}
          >
            <Typography color={theme.colors.white} size={isMobile ? 18 : 20}>
              {t("translation:accept")}
            </Typography>
          </AcceptBtn>
        </>
      )}
    </Container>
  );
};

export const Container = styled.div`
  top: 0px;
  left: 0px;
  position: fixed;
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: rgba(0, 0, 0, 0.8);
  z-index: 100;
  backdrop-filter: blur(5px);
  transition: 0.5s;

  @media (max-width: 500px) {
    padding: 0 40px;
  }
`;

export const PolicyLink = styled.a`
  margin-top: ${({ theme }) => theme.spacings.xlarge}px;
  margin-bottom: ${({ theme }) => theme.spacings.xxlarge}px;

  &:hover {
    cursor: pointer;
  }
`;

export const AcceptBtn = styled.button`
  padding: 15px 50px;
  border: 1px solid ${theme.colors.white};
  border-radius: 30px;
  background: transparent;
  position: relative;
  transition: transform 100ms ease-in;

  &:hover {
    cursor: pointer;
    transform: scale(1.1);
  }
`;
