import Link from 'next/link';
import * as React from 'react';
import { useState } from 'react';
import styled from 'styled-components';
import { Blog as BlogMoel } from '../../models/blog.model.shared';
import Typography from '../Typography/Typography';
import Image from 'next/image';
import theme from '../../lib/theme';

interface BlogProps {
  blog: BlogMoel;
  onClick?: (e?: any) => void;
}

const Blog: React.FC<BlogProps> = ({ blog, onClick }: BlogProps) => {
  const validateDate = (date: Date) => {
    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();

    var newDate = mm + '.' + dd + '.' + yyyy;
    return newDate;
  };

  return (
    <Link href={'/blog/123'}>
      <BlogContaier>
        <Author>
          <AuthorAvatar>
            <Image
              //   src={blog.author.avatar}
              src="/assets/favicon.png"
              alt="avatar"
              width={50}
              height={50}
              layout="responsive"
              placeholder="blur"
              blurDataURL="/assets/defaultImage/avatar.jpeg"
            />
          </AuthorAvatar>
          <div style={{ marginLeft: 15 }}>
            <Typography>Shouter Inc</Typography>
          </div>
        </Author>
        <div style={{ width: '100%' }}>
          <Typography
            spacingBefore={40}
            align="left"
            font="bold"
            size={theme.fontSizes.subHeader}
            color={theme.colors.grey8}
          >
            {blog.title}
          </Typography>

          <Typography
            spacingBefore={20}
            align="left"
            font="bold"
            size={theme.fontSizes.medium}
            color={theme.colors.grey6}
          >
            {validateDate(new Date(blog.date || ''))}
            {' \u2022 '}
            {blog.readingTime + 'm to read'}
          </Typography>
        </div>

        <ImageContaier>
          <Image
            src={blog.cover || '/assets/defaultImage/avatar.jpeg'}
            alt="avatar"
            layout="fill"
            placeholder="blur"
            objectFit="cover"
            blurDataURL="/assets/defaultImage/avatar.jpeg"
          />
        </ImageContaier>
      </BlogContaier>
    </Link>
  );
};

export default Blog;

export const BlogContaier = styled.div`
  min-width: 80px;
  min-height: 200px;
  background-color: ${({ theme }) => theme.colors.grey1};
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 25px;
  padding: 20px;
  box-shadow: 0px 10px 20px 0px rgba(0, 0, 0, 0.1);

  &:hover {
    cursor: pointer;
  }

  @media (max-width: 1100px) {
  }
`;

export const Author = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const AuthorAvatar = styled.div`
  border-radius: 50px;
  width: 50px;
  height: 50px;
  background-color: ${({ theme }) => theme.colors.grey2};
  overflow: hidden;
`;

export const ImageContaier = styled.div`
  position: relative;
  border-radius: 20px;
  margin-top: 10px;
  width: 100%;
  height: 350px;
  background-color: ${({ theme }) => theme.colors.grey2};
  overflow: hidden;
`;
