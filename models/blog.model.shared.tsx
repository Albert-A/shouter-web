import { AbstractModelShared } from './abstract.model.shared';
import { Author } from './author.model.shared';
import { Blok } from './blok.model.shared';

export class Blog extends AbstractModelShared {
  title?: string;
  subtitle?: string;
  cover?: string;
  readingTime?: number;
  author?: Author;
  date?: Date;
  content?: Blok[];
}

export type BlogAPI = Pick<Blog, 'id' | 'createdAt' | 'cover' | 'content'>;
