import { AbstractModelShared } from './abstract.model.shared';

export class Author extends AbstractModelShared {
  name?: string;
  avatar?: string;
  email?: number;
}
