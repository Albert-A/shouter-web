import { AbstractModelShared } from './abstract.model.shared';

export class Blok extends AbstractModelShared {
  type?: string;
  text?: string;
  date?: Date;
  state?: string;
  title?: string;
  image?: string;
}
